from flask import Flask
from flask_session import Session
from flask_sqlalchemy import SQLAlchemy
from config import config


def create_app(config_name):
    app = Flask(__name__)
    # 加载导入的模块Config，启用debug模式
    app.config.from_object(config[config_name])
    db = SQLAlchemy(app)
    Session(app)
    from info.modules.news import url_blue
    app.register_blueprint(url_blue)
    return app

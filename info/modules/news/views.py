from flask import render_template
from flask import session

from info.modules.news import url_blue


@url_blue.route("/")
def index():
    session['name'] = 'yu'
    return render_template('./news/index.html')
    # return 'index'

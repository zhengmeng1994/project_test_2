from flask import Flask, render_template, session
# 导入数据库模块
from flask_sqlalchemy import SQLAlchemy
# 导入script模块
from flask_script import Manager
from info import create_app
from flask_bootstrap import Bootstrap


app = create_app('develop')
manager = Manager(app)
Bootstrap(app)
if __name__ == "__main__":
    manager.run()

# 导入状态保持框架并将session存入redis数据库中
from flask_session import Session
import redis


class Config(object):

    # 建立数据库的连接
    SQLALCHEMY_DATABASE_URI = 'mysql://root:mysql@localhost/project1'
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    # db = SQLAlchemy(app)
    # 设置secret_key,将session的数据存储到redis数据库中
    SECRET_KEY = 'HFSKJDGJHG+JSDJOEI%=='
    # 实例化redis
    # sr = redis.StrictRedis(host='localhost', port=6379, db=1)

    # 指定sessions保存到redis数据库中
    SESSION_TYPE = 'redis'
    # 让cookie中的session被签名加密处理
    SESSION_USE_SIGNER = True
    # 使用redis实例化对象
    SESSION_HOST = 'localhost'
    # 定义一个变量存储固定的地址和端口，复用
    SESSION_PORT = 6379
    SESSION_REDIS = redis.StrictRedis(host=SESSION_HOST, port=SESSION_PORT, db=0)
    # 设置session的过期时间
    PERMANENT_SESSION_LIFETIME = 86400


class Develop(Config):
    DEBUG = True


class Production(Config):
    DEBUG = False


config = {
    'develop': Develop,
    'production': Production
}
